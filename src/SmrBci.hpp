#ifndef SMRBCI_HPP
#define SMRBCI_HPP

#include <Eigen/Dense>
#include <whicore/Laplacian.hpp>
#include <whicore/Window.hpp>
#include <whicore/Pwelch.hpp>
#include <whicore/Gaussian.hpp>
#include <whicore/RingBuffer.hpp>
#include <whicore/XMLReader.hpp>

#include <whibci/Bci.hpp>

#define FEATURES_DIM 	2

typedef struct {

	unsigned int 	nsamples;
	unsigned int 	nchannels;
	unsigned int 	frameshift;
	unsigned int 	fs;

	unsigned int 	wlength;
	unsigned int	wtype;
	unsigned int 	novl;
	bool		dolog;

	float 		rejection;
	float		integration;

	std::string	lapfile;
	std::string 	gaufile;

} smrconfig_t;


class SmrBci : public Bci {

	public:
		SmrBci(void);
		~SmrBci(void);

		int Setup(smrconfig_t smrconf);
		int Run(void);
		
		void Dump(void);

	
	public:

		unsigned int 	nsamples;
		unsigned int 	nchannels;
		unsigned int 	fs;

		Laplacian* 	laplacian;	
		Pwelch*		pwelch;
		Gaussian* 	gaussian;
		
		Eigen::MatrixXd psd;
		Eigen::VectorXd	pp;
	
		
		
};


#endif
