#ifndef BCISMR_CPP
#define BCISMR_CPP

#include "SmrBci.hpp"

SmrBci::SmrBci(void) {

	// Instansiation of objects
	this->_buffer		= new RingBuffer();
	this->laplacian 	= new Laplacian();
	this->pwelch 		= new Pwelch();
	this->gaussian 		= new Gaussian();

};

SmrBci::~SmrBci(void) {
	
	if (this->_buffer != NULL)
		free(this->_buffer);
	
	if (this->laplacian != NULL)
		free(this->laplacian);

	if (this->pwelch != NULL)
		free(this->pwelch);
	
	if (this->gaussian != NULL)
		free(this->gaussian);
};

int SmrBci::Setup(smrconfig_t smrconf) {
	
	this->nsamples 	= smrconf.nsamples;
	this->nchannels = smrconf.nchannels;
	this->fs	= smrconf.fs;

	this->_buffer->Setup(smrconf.nsamples, smrconf.nchannels);

	this->laplacian->Setup(smrconf.lapfile.c_str());

	this->gaussian->Setup(smrconf.gaufile.c_str());

	this->pwelch->Setup(smrconf.wlength, smrconf.wtype, smrconf.novl, smrconf.fs, smrconf.dolog);
	
	// Initialize output members
	this->psd 	= Eigen::MatrixXd::Zero(this->pwelch->config.nfft, this->nchannels);
	this->pp 	= Eigen::VectorXd::Zero(this->gaussian->config.nclasses);
}

int SmrBci::Run(void) {

	unsigned int nsamples;
	unsigned int nchannels;
	unsigned int nfeatures;

	Eigen::MatrixXd cbuffer;
	Eigen::MatrixXd lbuffer;
	Eigen::MatrixXd cpsd;
	Eigen::VectorXd cfeat;
	Eigen::MatrixXi featId;
	Eigen::VectorXd cpp;

	cbuffer = Eigen::MatrixXd::Zero(this->nsamples, this->nchannels);
	lbuffer = Eigen::MatrixXd::Zero(this->nsamples, this->nchannels);
	cfeat   = Eigen::VectorXd::Zero(this->gaussian->config.nfeatures);

	// Get data	
	this->_buffer->Get(cbuffer);

	// Apply Laplacian filter
	this->laplacian->Apply(cbuffer, lbuffer);
		
	// Apply pwelch
	this->pwelch->Apply(lbuffer); 
	
	// Getting psd
	this->pwelch->Get(this->psd);

	// Getting selected features
	this->pwelch->Get(cfeat, this->gaussian->config.idchan, this->gaussian->config.idfreq);

	// Run classifier
	this->gaussian->Run(cfeat, this->pp);
};

void SmrBci::Dump(void) {

	printf("[SmrBci] - Configuration\n");
	printf(" 	 |+ EEG data:\n");
	printf(" 	  |- Sampling rate: 		%d\n", this->fs);
	printf(" 	  |- Number of channels: 	%d\n", this->nchannels);
	printf(" 	  |- Buffer size: 		%d\n", this->nsamples);
	this->laplacian->Dump();	
	this->pwelch->Dump();
	this->gaussian->Dump();
	printf("\n");
}

#endif
