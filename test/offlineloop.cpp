#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <iostream>
#include <string>

#include <Eigen/Dense>
#include <whicore/XDFReader.hpp>
#include <whicore/Rejector.hpp>
#include <whicore/Exponential.hpp>
#include <whicore/wc_ioutilities.hpp>
#include <whicore/bcitk_utilities.hpp>

#include "SmrBci.hpp"

#define ROOTPATH 	getenv("WHISMR")
#define SUBPATH 	"extra/offlineloop/"

#define BUFFER_SIZE	512
#define FRAME_SIZE	32

using namespace Eigen;
using namespace std;

int hevt_reset[3] = {7, 9 , 10};
int sevt_reset[3] = {781, 898, 897};

bool check_reset_hw(const Ref<const MatrixXi>& tframe, int* values, int nval) {

	unsigned int i;
	bool evtreset = false;
	vector<int> pos;
	MatrixXi frame;

	frame = tframe.transpose();

	for (i = 0; i<nval; i++) {
		if(bcitk_gettrigger(frame, pos, values[i]))
			evtreset = evtreset || true;
	}

	return evtreset;
};

bool get_event(vector<xdfevent> evtlist, xdfevent * evt, unsigned int frameId, unsigned int framesize) {

	bool retcod = false;
	std::vector<xdfevent>::iterator it;

	for (it = evtlist.begin(); it < evtlist.end(); it++) {
		if(floor(((*it).evtpos)/float(framesize)) == frameId) {
			evt->evtpos = (*it).evtpos;
			evt->evtcod = (*it).evtcod;
			evt->evtdur = (*it).evtdur;
			retcod = true;
		}
	}

	return retcod;
}

bool check_reset_sw(xdfevent *evt, int* values, int nval) {

	unsigned int i;
	bool evtreset = false;

	for (i = 0; i<nval; i++) {
		if(evt->evtcod == values[i])
			evtreset = evtreset || true;
	}

	return evtreset;	
}

smrconfig_t get_smrconfiguration(unsigned int nsamples, unsigned int nchannels, unsigned int frameshift, unsigned int fs, string xmlfile) {
	
	smrconfig_t 	smrconf;
	XMLReader 	xml;
	
	xml.Import(xmlfile.c_str());

	smrconf.nsamples 	= nsamples;
	smrconf.nchannels 	= nchannels;
	smrconf.frameshift 	= frameshift;
	smrconf.fs 		= fs;
	
	smrconf.wlength 	= xml.GetInt("pwelch/winsize");
	smrconf.wtype   	= Window::AsHamming;
	smrconf.novl 		= xml.GetInt("pwelch/novl");
	smrconf.dolog  		= xml.GetBool("pwelch/dolog");

	smrconf.rejection	= xml.GetFloat("rejection");
	smrconf.integration 	= xml.GetFloat("integration/exponential"); 

	smrconf.lapfile 	= xml.GetString("filter/laplacian/");
	smrconf.gaufile 	= xml.GetString("classifier/gaussian/");

	return smrconf;
}


int main(int argc, char **argv) {

	char 		idata[256], ixml[256], opsd[256], opp[256], orpp[256], oipp[256];
	unsigned int 	NumSamplesTot, NumSamples, NumChannels, NumClasses;
	unsigned int  	Nfft, NumFrames, SampleRate, FrameShift;
	unsigned int 	sstart, i, nsevt, nhevt, nsreset;
	float 		RejValue, IntValue;
	
	float 		*frame;
	MatrixXf 	eeg, cframe;
	MatrixXi	tri, tframe;
	MatrixXd 	cbuffer, cpsd, psd, pp, rpp, ipp;
	VectorXd	cpp, crpp, cipp;
	
	SmrBci*		bci;
	Rejector*	reject;
	Exponential*	accum;

	vector<xdfevent> evtlist;
	xdfevent 	cevt;
	
	smrconfig_t smrconf;

	sprintf(idata, "%s%sofflineloop.gdf", ROOTPATH, SUBPATH);
	sprintf(ixml, "%s%sofflineloop.xml", ROOTPATH, SUBPATH);
	sprintf(opsd,  "%s%sofflineloop_psd.dat", ROOTPATH, SUBPATH);
	sprintf(opp,   "%s%sofflineloop_pp.dat",  ROOTPATH, SUBPATH);
	sprintf(orpp,  "%s%sofflineloop_rpp.dat", ROOTPATH, SUBPATH);
	sprintf(oipp,  "%s%sofflineloop_ipp.dat", ROOTPATH, SUBPATH);


	// Importing gdf file
	XDFReader xdf;
	
	if(xdf.Open(idata) < 0)
		goto exit;	

	xdf.Import();
	xdf.Dump();
	xdf.Get(eeg, XDFR_EEG); // Remember: cols->samples, rows->channels
	xdf.GetTrigger(tri);
	evtlist = xdf.GetEvents();

	// Import xml file and read smr configuration
	smrconf = get_smrconfiguration(BUFFER_SIZE, xdf.GetNumEEG(), 
						FRAME_SIZE, xdf.GetFrequency(), ixml);

	NumSamplesTot  	= xdf.GetNumSamples(); 
	NumChannels 	= xdf.GetNumEEG();
	NumSamples 	= BUFFER_SIZE;
	FrameShift 	= FRAME_SIZE;
	RejValue	= smrconf.rejection;
	IntValue	= smrconf.integration;

	// Initialize bci 
	bci = new SmrBci(); 
	bci->Setup(smrconf);

	NumFrames = floor(NumSamplesTot/(float)FrameShift);
	frame 		= (float*)malloc(sizeof(float)*FrameShift*NumChannels);
	Nfft		= bci->pwelch->config.nfft;
	NumClasses	= bci->gaussian->config.nclasses;

	cbuffer 	= MatrixXd::Zero(NumSamples, NumChannels);
	cframe 		= MatrixXf::Zero(NumChannels, FrameShift);
	tframe 		= MatrixXi::Zero(1, FrameShift);
	cpp 		= VectorXd::Zero(NumClasses);
	crpp 		= VectorXd::Zero(NumClasses);
	cipp 		= VectorXd::Zero(NumClasses);
	cpsd    	= MatrixXd::Zero(Nfft,  NumChannels); 
	
	// Initialize rejector and accumulator
	reject 	= new Rejector(RejValue, NumClasses, Rejector::AsReplace);
	accum 	= new Exponential(IntValue, NumClasses);
	
	// Initialize storage parameters
	psd    		= MatrixXd::Zero(NumFrames*Nfft, NumChannels); 
	pp 		= MatrixXd::Zero(NumFrames, NumClasses); 
	rpp 		= MatrixXd::Zero(NumFrames, NumClasses); 
	ipp 		= MatrixXd::Zero(NumFrames, NumClasses); 
	pp.fill(0.5);
	rpp.fill(0.5);
	ipp.fill(0.5);
	
	nsevt = 0;
	nhevt = 0;
	nsreset = 0;

	bci->Dump();
	// Starting simulated bci loop
	printf("[SmrBci] - Starting loop\n");
	for(i = 0; i < NumFrames; i++) {

		sstart = FrameShift * i;
		
		// Get data - get data block of 32 from the eeg
		cframe = eeg.middleCols(sstart, FrameShift);
		tframe = tri.middleCols(sstart, FrameShift);

		// Simulating pipe reading -> getting float array
		Map<MatrixXf>(frame, NumChannels, FrameShift) = cframe;

		// Adding frame to BCI buffer
		bci->AddFrame(frame, cframe.cols(), cframe.rows());
		
		// Check if BCI is ready
		if(!bci->IsReady()) { continue; }

		// Run SMR bci
		bci->Run();
	
		// Get bci psd
		bci->pwelch->Get(cpsd);

		// Get bci output (probabilities)
		cpp = bci->pp;
		
		
		// Apply rejection
		reject->Apply(cpp, crpp);
		
		// Apply integration
		accum->Apply(crpp, cipp);


		if(check_reset_hw(tframe, hevt_reset, 3)) {
			accum->Reset();
			nhevt++;
		}

		if(get_event(evtlist, &cevt, i, FrameShift)) {
			if(check_reset_sw(&cevt, sevt_reset, 3)) {
				accum->Reset();
				nsreset++;
			}

			nsevt++;
		}

		// Store current integrated probabilities in the rejection
		reject->SetValues(cipp);
	
		// Storing results
		psd.middleRows(i*Nfft, Nfft) = cpsd;
		pp.row(i) = cpp;
		rpp.row(i) = crpp;
		ipp.row(i) = cipp;
	}

	printf("[SmrBci] - Number software events: %d\n", nsevt);
	printf("[SmrBci] - Number software reset events: %d\n", nsreset);
	printf("[SmrBci] - Number hardware reset events: %d\n", nhevt);

	printf("[SmrBci] - Number of samples: %d\n", NumSamples);
	printf("[SmrBci] - Number of iterations: %d\n", NumFrames);
	printf("[SmrBci] - Saving data at:\n");
	printf("      + Power spectral density: 			%s\n", opsd);
	printf("      + Raw posterior probabilities:			%s\n", opp);
	printf("      + Posterior probabilities with rejection:		%s\n", orpp);
	printf("      + Posterior probabilities with integration:	%s\n", oipp);

	wc_save(opsd, psd, "data", "pwelch");
	wc_save(opp, pp, "data", "raw probabilities");
	wc_save(orpp, rpp, "data", "rejected probabilities");
	wc_save(oipp, ipp, "data", "integrated probabilities");

exit:
	free(frame);
	xdf.Close();
	return 0;

}
