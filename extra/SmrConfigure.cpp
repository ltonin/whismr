#ifndef SMRCONFIGURE_CPP
#define SMRCONFIGURE_CPP

#include "SmrConfigure.hpp"
#include <whicore/Window.hpp>

SmrConfigure::SmrConfigure(void) {};

SmrConfigure::~SmrConfigure(void) {};


int SmrConfigure::Import(const char* xmlfile) {
	
	int retcod;
	tinyxml2::XMLNode 	*pRoot;
	tinyxml2::XMLElement 	*pElement;
	tinyxml2::XMLElement 	*pDeviceElement;
	
	std::string lap;

	retcod = this->_xmlfile.LoadFile(xmlfile);
	if (retcod > 0) return tinyxml2::XML_ERROR_FILE_NOT_FOUND;

	//pRoot = this->_xmlfile.FirstChild();
	//if(pRoot == NULL) return tinyxml2::XML_ERROR_EMPTY_DOCUMENT;
	
	this->_smrconf.samplerate 	= this->GetInt("device/samplerate");
	this->_smrconf.nchannels  	= this->GetInt("device/layout/neeg");
	this->_smrconf.bsize 	  	= this->GetInt("process/buffer/size");
	this->_smrconf.psd_winsize 	= this->GetInt("process/pwelch/winsize");
	this->_smrconf.psd_wintype 	= this->GetInt("process/pwelch/wintype");
	this->_smrconf.psd_novl 	= this->GetInt("process/pwelch/novl");
	this->_smrconf.psd_nfft		= (this->_smrconf.psd_winsize/2.0) + 1;
	this->_smrconf.psd_dolog 	= this->GetInt("process/pwelch/dolog");

	lap = this->GetString("process/filter/laplacian");
	this->_smrconf.lap_path = (char*)malloc(lap.length()*sizeof(char));
	sprintf((char*)this->_smrconf.lap_path, "%s", lap.c_str()); 

	//// Device
	//pElement = pRoot->FirstChildElement("device");
	//if (pElement == NULL) return tinyxml2::XML_ERROR_PARSING_ELEMENT;
	//devtyp = pElement->Attribute("type");

	//pElement = pRoot->FirstChildElement("device")->FirstChildElement("samplerate");
	//pElement->QueryIntText(&samplerate);
	//
	//pElement = pRoot->FirstChildElement("device")->FirstChildElement("layout")->FirstChildElement("neeg");
	//pElement->QueryIntText(&neeg);
	//
	//pElement = pRoot->FirstChildElement("device")->FirstChildElement("layout")->FirstChildElement("nexg");
	//pElement->QueryIntText(&nexg);

	//pElement = pRoot->FirstChildElement("device")->FirstChildElement("layout")->FirstChildElement("ntri");
	//pElement->QueryIntText(&ntri);

	//// TO DO channel labels
	//
	//pElement = pRoot->FirstChildElement("process")->FirstChildElement("buffer")->FirstChildElement("size");
	//pElement->QueryIntText(&bsize);
	//
	//pElement = pRoot->FirstChildElement("process")->FirstChildElement("buffer")->FirstChildElement("frame");
	//pElement->QueryIntText(&fsize);
	//
	//pElement = pRoot->FirstChildElement("process")->FirstChildElement("filter")->FirstChildElement("laplacian");
	//lap = pElement->Attribute("path");

	//pElement = pRoot->FirstChildElement("process")->FirstChildElement("pwelch")->FirstChildElement("winsize");
	//pElement->QueryIntText(&pwinsize);
	//
	////pElement = pRoot->FirstChildElement("process")->FirstChildElement("pwelch")->FirstChildElement("wintype");
	////pElement->GetText(&pwintyp);

	//pElement = pRoot->FirstChildElement("process")->FirstChildElement("pwelch")->FirstChildElement("novl");
	//pElement->QueryIntText(&pnovl);
	//
	//pElement = pRoot->FirstChildElement("process")->FirstChildElement("pwelch")->FirstChildElement("dolog");
	//pElement->QueryIntText(&pdolog);

	//this->_smrconf.lap_path = (char*)malloc(lap.length()*sizeof(char));
	//sprintf((char*)this->_smrconf.lap_path, "%s", lap.c_str()); 


}



void SmrConfigure::Dump(void) {

	printf("[SmrConfigure] - Dump imported configuration\n");
	printf("| + EEG data:\n");
	printf("  | - Sampling rate: %d\n", this->_smrconf.samplerate);
	printf("  | - Number of channels: %d\n", this->_smrconf.nchannels);
	printf("  | - Buffer size: %d\n", this->_smrconf.bsize);
	printf("| + Laplacian:\n");
	printf("  | - Layout path: %s\n", this->_smrconf.lap_path);
	printf("| + Pwelch:\n");
	printf("  | - Pwelch window size: %d\n", this->_smrconf.psd_winsize);
	printf("  | - Pwelch window type: %d\n", this->_smrconf.psd_wintype);
	printf("  | - Pwelch window overlap: %d\n", this->_smrconf.psd_novl);
	printf("  | - Pwelch nfft: %d\n", this->_smrconf.psd_nfft);
	printf("  | - Pwelch dolog: %d\n", this->_smrconf.psd_dolog);


}


#endif
