#ifndef SMRCONFIGURE_HPP
#define SMRCONFIGURE_HPP

#include <tinyxml2.h>
#include <whibci/Configure.hpp>

#include "SmrBci.hpp"

class SmrConfigure : public Configure {

	public:
		SmrConfigure(void);
		~SmrConfigure(void);
			
		int Import(const char* xmlfile);
		void Dump(void);
	
	private:
		smrconf_t	_smrconf;

};


#endif
